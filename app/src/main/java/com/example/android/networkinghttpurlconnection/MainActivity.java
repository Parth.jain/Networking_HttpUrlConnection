package com.example.android.networkinghttpurlconnection;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private ConnectivityManager mConnMgr;

    ImageView mLoadedImage;
    public String sPreferredNetwork;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLoadedImage = findViewById(R.id.loadedImage);

        mConnMgr=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        Button mLoadImage=findViewById(R.id.loadBtn);
        mLoadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshImage();
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(this);

        sPreferredNetwork=preferences.getString("ChooseNetworkType","Any");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_setting){
            Intent settingIntent=new Intent(getBaseContext(),SettingsActivity.class);
            startActivity(settingIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void refreshImage() {
      //  final String TAG="Refresh";
        String imagePath="https://placeimg.com/640/480/any";

        boolean isWifiAvailable=mConnMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();

        if(sPreferredNetwork.equals("Any")){
            if(mConnMgr!=null){
                NetworkInfo networkInfo=mConnMgr.getActiveNetworkInfo();
                if (networkInfo!=null && networkInfo.isConnected()){
                    new DownloadImageTask().execute(imagePath);
                }
                else{
                    Toast.makeText(this,"Network not available",Toast.LENGTH_LONG).show();
                }
            }
            else if(sPreferredNetwork.equals("WiFi")){
                if(isWifiAvailable){
                    new DownloadImageTask().execute(imagePath);
                }
                else{
                   Toast.makeText(this,"Data Allowed only on wifi Network",Toast.LENGTH_LONG).show();

                }

            }
            else{
                Toast.makeText(this,"Data disabled by user",Toast.LENGTH_LONG).show();
            }
        }
//
//        if(mConnMgr!=null)
//        {
//            NetworkInfo networkInfo=mConnMgr.getActiveNetworkInfo();
//            if(networkInfo!=null && networkInfo.isConnected()){
//                new DownloadImageTask().execute(imagePath);
//                Log.i(TAG,"Done");
//            }
//            else{
//                Toast.makeText(this,"Network not available",Toast.LENGTH_LONG).show();
//            }
//
//        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            return downloadImage(strings[0]);
        }

        private Bitmap downloadImage(String path) {
            final String TAG="Download Task";
            Bitmap bitmap=null;
            InputStream inputStream;
            try{
                URL url=new URL(path);
                HttpURLConnection urlConn=(HttpURLConnection)url.openConnection();
                //urlConn.setConnectTimeout(5000);
                //urlConn.setReadTimeout(2500);
                urlConn.setRequestMethod("GET");
                urlConn.setDoInput(true);

                urlConn.connect();
                inputStream=urlConn.getInputStream();
                bitmap=BitmapFactory.decodeStream(inputStream);
                Log.i(TAG,"Download Task Completed");


            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(mLoadedImage!=null){
                mLoadedImage.setImageBitmap(bitmap);
                mLoadedImage.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        }
    }
}
